
export default {
    sideMenuComponents() {
        const componentList = [
            {
                name: "Agenda",
                title: "Agenda",
                icon: "calendar",
                tag: "agenda",
                active: true
            },
            {
                name: "Resources",
                title: "Resources",
                icon: "file",
                tag: "resources",
                active: true
            },
            {
                name: "Chat",
                title: "Chat",
                icon: "comment-alt",
                tag: "chat",
                active: true
            },
            {
                name: "Notes",
                title: "Notes",
                icon: "sticky-note",
                tag: "notes",
                active: true
            },
            {
                name: "Polls",
                title: "Polls",
                icon: "poll",
                tag: "polls",
                active: true
            },
            {
                name: "Qa",
                title: "Q&A",
                icon: "comments",
                tag: "qa",
                active: true
            },
        ]
        return componentList;
    },
}