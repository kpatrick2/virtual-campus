import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    FontAwesomeIcon,
    FontAwesomeLayers,
    FontAwesomeLayersText,
} from '@fortawesome/vue-fontawesome';


import {
    faCalendar as fasCalendar,
    faPoll as fasPoll,
    faStickyNote as fasStickyNote,
    faCommentAlt as fasCommentAlt,
    faFile as fasFile,
    faComments as fasComments,
    faCompress as fasCompress,
    
} from '@fortawesome/free-solid-svg-icons';

library.add(
    fasCalendar,
    fasPoll,
    fasStickyNote,
    fasCommentAlt,
    fasFile,
    fasComments,
    fasCompress
);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);
Vue.component('font-awesome-layers-text', FontAwesomeLayersText);

Vue.config.productionTip = false;